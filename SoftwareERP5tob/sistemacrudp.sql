-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 26-03-2019 a las 03:25:57
-- Versión del servidor: 10.1.34-MariaDB
-- Versión de PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sistemacrudp`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `ID` int(10) NOT NULL,
  `CODIGO` varchar(15) NOT NULL,
  `Fecha` int(11) NOT NULL,
  `Proveedor` varchar(30) NOT NULL,
  `Producto` varchar(50) NOT NULL,
  `Cantidad` int(15) NOT NULL,
  `Precio` int(11) NOT NULL,
  `IVA` int(11) NOT NULL,
  `Descuento` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`ID`, `CODIGO`, `Fecha`, `Proveedor`, `Producto`, `Cantidad`, `Precio`, `IVA`, `Descuento`) VALUES
(1, '10', 1996, 'AUROS', 'Pc', 100, 500, 12, 10),
(2, '10000', 2018, 'MSI', 'Motherboard', 2, 10, 14, 2),
(3, '456', 2000, 'Intel', 'Impresora', 10000, 400, 12, 4),
(4, '123', 2003, 'ALV', 'ERc', 50, 10, 12, 10),
(5, '11', 1996, 'Gfes', 'Alex', 1, 100000000, 0, 0),
(6, '45', 2013, 'Corsair', 'RAM DDR4', 600, 150, 12, 2),
(7, '1234', 2019, 'MSI', 'PC laptop', 10, 1023, 9, 2),
(8, '89', 2017, 'Intel', 'Pc', 800, 600, 0, 0),
(9, '6', 1005, 'Pc', 'Estidiante', 40, 500, 0, 0),
(10, '40', 7893, 'B', 'A', 10, 10, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedores`
--

CREATE TABLE `proveedores` (
  `ID` int(11) NOT NULL,
  `Ruc` int(15) NOT NULL,
  `Nombre` varchar(15) NOT NULL,
  `Direccion` varchar(50) NOT NULL,
  `telefono` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedores`
--

INSERT INTO `proveedores` (`ID`, `Ruc`, `Nombre`, `Direccion`, `telefono`) VALUES
(1, 50, 'juanito', 'Propi', '2151515'),
(8, 78, 'Pacman', 'USA', '798952'),
(9, 45, 'Nautic dogs', 'Alemania', '45621'),
(10, 456, 'Bugisoft', 'Ricauter', '4564'),
(11, 4562, 'EsmeGames', 'Esmeraldas', '596321'),
(12, 7925, 'EuroGamers', 'España', '506983'),
(13, 36, 'Epic Game', 'Trulia', '2646946'),
(14, 461, 'Gamer SA', 'Reino unido', '26341616'),
(15, 77, 'AUROS', 'California', '500034'),
(16, 7, 'AMD', 'California', '5103164');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`ID`);

--
-- Indices de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `proveedores`
--
ALTER TABLE `proveedores`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
