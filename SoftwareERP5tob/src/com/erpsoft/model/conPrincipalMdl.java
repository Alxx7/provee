
package com.erpsoft.model;
import com.erpsoft.view.conPrincipalVw;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class conPrincipalMdl {
  Connection cc;
  Connection cn= Conexion();
  conPrincipalVw V = new conPrincipalVw();
  public Connection Conexion(){
      try{
         Class.forName("com.mysql.jdbc.Driver");
         cc= DriverManager.getConnection("jdbc:mysql://localhost/sistemacrudp","root","");
         System.out.println("Conexión establecida");
      }catch(Exception e){
        System.out.println(e);  
      }
      return cc;
  } 
  
  public void iniciarModelo(){
      Bloqueo();
      MostrarProveedores("");
      Mostrarordendecompra("");
      Bloqueo1();
      
  }
  
  public void EliminarProveeedor(){
  NuevoProveedor();
       conPrincipalVw.btnAgregar.setEnabled(false);
        conPrincipalVw.btnModificar.setEnabled(true);
        int fila= conPrincipalVw.tabla.getSelectedRow();
        if (fila>0) {
            String ID= conPrincipalVw.tabla.getValueAt(fila,0).toString();
            try{
            PreparedStatement ppt= cn.prepareStatement("DELETE FROM Proveedores  WHERE ID='"+ID+"'");
            ppt.executeUpdate();
            JOptionPane.showConfirmDialog(null, "Seguro que quieres eliminar este proveedor?");
            JOptionPane.showMessageDialog(null, "Se elimino con exito, así como ella te elimino de su vida :'v "); 
            MostrarProveedores("");
           }catch(SQLException ex){
              JOptionPane.showMessageDialog(null, "Algo hiciste mal");     
           }
        }else{
           JOptionPane.showMessageDialog(null, "Selecciona la fila CRACK");        
        }    
  }
  
  public void ActualizarProveedor(){
      try{
          
            String ID= conPrincipalVw .buscartxt1.getText();
            String Ruc= conPrincipalVw.Ructxt.getText();
            String Nombre= conPrincipalVw.nombretxt.getText();
            String Direccion= conPrincipalVw.Direcciontxt.getText();
            String Telefono= conPrincipalVw.Direcciontxt.getText();
            
            if (Ruc.equals("") ||  Nombre.equals("") ||   Direccion.equals("") || Telefono.equals("")) {
                JOptionPane.showMessageDialog(null, " ");
            }else{  
              PreparedStatement  ppt= cn.prepareStatement("UPDATE Proveedores SET "
                      + " Ruc = '"+Ruc+"',"
                      + "Nombre='"+Nombre+"',"
                      + "Direccion='"+Direccion+"',"
                      + "telefono= '"+Telefono+"'"
                      + "WHERE ID='"+ID+"'");   
              ppt.executeUpdate();
               JOptionPane.showMessageDialog(null, "Proveedor modificado");  
               MostrarProveedores("");
               Bloqueo();
            }
        }catch(Exception e){
       JOptionPane.showMessageDialog(null, "Esta mal men");           
        } 
  }
  
    public void Consultarproveedores(){
      NuevoProveedor();
       conPrincipalVw.btnAgregar.setEnabled(false);
        conPrincipalVw.btnModificar.setEnabled(true);
        int fila= conPrincipalVw.tabla.getSelectedRow();
        if (fila>0) {
           conPrincipalVw.buscartxt1.setText(conPrincipalVw.tabla.getValueAt(fila, 0).toString());
           conPrincipalVw.Ructxt.setText(conPrincipalVw.tabla.getValueAt(fila, 1).toString());
           conPrincipalVw.nombretxt.setText(conPrincipalVw.tabla.getValueAt(fila, 2).toString());
           conPrincipalVw.Direcciontxt.setText(conPrincipalVw.tabla.getValueAt(fila, 3).toString());
           conPrincipalVw.Telefonotxt.setText(conPrincipalVw.tabla.getValueAt(fila, 4).toString());
        }else{
           JOptionPane.showMessageDialog(null, "Selecciona la fila CRACK");        
        }
    }
    
    public static void main(String[] args) {
        conPrincipalMdl m= new conPrincipalMdl();
        m.Conexion();
    }
    
    public void limpiar(){
          conPrincipalVw.Ructxt.setText("");
        conPrincipalVw.nombretxt.setText("");
        conPrincipalVw.Direcciontxt.setText("");
        conPrincipalVw.Telefonotxt.setText("");
      
    }
    
    public void MostrarProveedores(String Valor){
        DefaultTableModel modo= new DefaultTableModel();
        modo.addColumn("ID");
        modo.addColumn("Ruc");
        modo.addColumn("Nombre");
        modo.addColumn("Direccion");
        modo.addColumn("Telefono");
        conPrincipalVw.tabla.setModel(modo);
        
            String sql;
            if (Valor.equals("")) {
            sql="SELECT* FROM Proveedores";
        }else{
         sql="SELECT* FROM Proveedores WHERE ID='"+Valor+"'";       
    } 
            String datos[]= new String [6];
            try{
            Statement st= cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                modo.addRow(datos);
                }
            conPrincipalVw.tabla.setModel(modo);
            }catch(SQLException ex){
             JOptionPane.showMessageDialog(null, "No se pudo mostar ni maiz"+ex);    
            }
    }
    
    public void AgregarProveedor(){
        try{
            String Ruc= conPrincipalVw.Ructxt.getText();
            String Nombre= conPrincipalVw.nombretxt.getText();
            String Direccion= conPrincipalVw.Direcciontxt.getText();
            String Telefono= conPrincipalVw.Direcciontxt.getText();
            
            if (Ruc.equals("") ||  Nombre.equals("") ||   Direccion.equals("") || Telefono.equals("")) {
                JOptionPane.showMessageDialog(null, "Pendeja ingresa valores antes de agregar");
            }else{
            PreparedStatement  ppt= cn.prepareStatement("INSERT INTO  Proveedores(Ruc,Nombre,Direccion,telefono  ) VALUES (?,?,?,?)");
          ppt.setString(1, conPrincipalVw.Ructxt.getText());
          ppt.setString(2, conPrincipalVw.nombretxt.getText());
          ppt.setString(3, conPrincipalVw.Direcciontxt.getText());
          ppt.setString(4, conPrincipalVw.Telefonotxt.getText());
          ppt.executeUpdate();
    
          JOptionPane.showMessageDialog(null,"Ufff Agregado crack");
             limpiar();
        }
             }catch(SQLException ex){
          JOptionPane.showMessageDialog(null,"YA VALIO VERDURA"+ex); 
        }
          
    }
    
    public void NuevoProveedor(){
        limpiar();
         conPrincipalVw.Ructxt.setEditable(true);
        conPrincipalVw.nombretxt.setEditable(true);
        conPrincipalVw.Direcciontxt.setEditable(true);
        conPrincipalVw.Telefonotxt.setEditable(true);
        conPrincipalVw.btnAgregar.setEnabled(true);
        conPrincipalVw.btnModificar.setEnabled(true);
        conPrincipalVw.btnCancelar.setEnabled(true); 
    }
    
    public void Bloqueo(){
        conPrincipalVw.Ructxt.setEditable(false);
        conPrincipalVw.nombretxt.setEditable(false);
        conPrincipalVw.Direcciontxt.setEditable(false);
        conPrincipalVw.Telefonotxt.setEditable(false);
        conPrincipalVw.btnAgregar.setEnabled(false);
        conPrincipalVw.btnModificar.setEnabled(false);
        conPrincipalVw.btnCancelar.setEnabled(false);
       // vista.Productotxt.setEditable(false);
        
        limpiar();
        
    }
    
    //codigo para orden de compra//
    
    public void Eliminarordendecompra(){
  Nuevaordendecompra();
       conPrincipalVw.btnAgregar2.setEnabled(false);
        conPrincipalVw.btnModificar2.setEnabled(true);
        int fila= conPrincipalVw.Tabla2.getSelectedRow();
        if (fila>0) {
            String ID= conPrincipalVw.Tabla2.getValueAt(fila,0).toString();
            try{
            PreparedStatement ppt= cn.prepareStatement("DELETE FROM compra WHERE ID='"+ID+"'");
            ppt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Se elimino con exito:'v "); 
            Mostrarordendecompra("");
           }catch(SQLException ex){
              JOptionPane.showMessageDialog(null, "Algo hiciste mal");     
           }
        }else{
           JOptionPane.showMessageDialog(null, "Selecciona la fila ");        
        }    
  }
    
    public void Actualizarordendecompra(){
      try{
                                                                                                                                                                                                                                                                                                                                                      String ID= conPrincipalVw.buscartxt15.getText();
              String Codigo= conPrincipalVw.Codigotxt.getText(); 
              String Fecha= conPrincipalVw.fechatxt.getText();
              String Proveedor= conPrincipalVw.Proveedortxt.getText();
              String Producto= conPrincipalVw.Productotxt.getText();
              String Cantidad= conPrincipalVw.Cantidadtxt.getText();
              String Precio= conPrincipalVw.Preciotxt.getText();
              String Iva= conPrincipalVw.Ivatxt.getText();
              String Descuento= conPrincipalVw.Descuentotxt.getText();
           
            
            
            if (Producto.equals("") ||  Codigo.equals("") ||   Cantidad.equals("") || Precio.equals("") || Fecha.equals("") || Iva.equals("") || Descuento.equals("") || Proveedor.equals("")) {
                JOptionPane.showMessageDialog(null, "ingresa valores antes de agregar");
            }else{  
              PreparedStatement  ppt= cn.prepareStatement("UPDATE compra SET "
                     + "CODIGO = '"+Codigo+"',"
                      + "Fecha='"+Fecha+"',"
                      + "Proveedor='"+Proveedor+"',"
                      + "Producto='"+Producto+"',"   
                      + "Cantidad='"+Cantidad+"',"         
                      + "Precio= '"+Precio+"',"
                      + "Iva='"+Iva+"',"    
                     + "Precio= '"+Descuento+"'"          
                      + "WHERE ID='"+ID+"'");   
              ppt.executeUpdate();
               JOptionPane.showMessageDialog(null, "orden de compra modificado");  
               Mostrarordendecompra("");
               Bloqueo();
            }
        }catch(Exception e){
       JOptionPane.showMessageDialog(null, "Esta mal"+e);           
        } 
  }
    
     public void Consultarordendecompra(){
      Nuevaordendecompra();
       conPrincipalVw.btnAgregar2.setEnabled(false);
        conPrincipalVw.btnModificar2.setEnabled(true);
        int fila= conPrincipalVw.Tabla2.getSelectedRow();
        if (fila>0) {
           conPrincipalVw.buscartxt15.setText(conPrincipalVw.Tabla2.getValueAt(fila, 0).toString());
           conPrincipalVw.Productotxt.setText(conPrincipalVw.Tabla2.getValueAt(fila, 1).toString());
           conPrincipalVw.Codigotxt.setText(conPrincipalVw.Tabla2.getValueAt(fila, 2).toString());
           conPrincipalVw.Cantidadtxt.setText(conPrincipalVw.Tabla2.getValueAt(fila, 3).toString());
           conPrincipalVw.Preciotxt.setText(conPrincipalVw.Tabla2.getValueAt(fila, 4).toString());
           conPrincipalVw.fechatxt.setText(conPrincipalVw.Tabla2.getValueAt(fila, 5).toString());
         conPrincipalVw.Ivatxt.setText(conPrincipalVw.Tabla2.getValueAt(fila, 6).toString());
         conPrincipalVw.Descuentotxt.setText(conPrincipalVw.Tabla2.getValueAt(fila,7).toString());
         conPrincipalVw.Proveedortxt.setText(conPrincipalVw.Tabla2.getValueAt(fila,8).toString());
        }else{
           JOptionPane.showMessageDialog(null, "Selecciona la fila");        
        }
    }
     
     public void limpiar1(){
          conPrincipalVw.Productotxt.setText("");
        conPrincipalVw.Codigotxt.setText("");
        conPrincipalVw.Cantidadtxt.setText("");
        conPrincipalVw.Preciotxt.setText("");
        conPrincipalVw.fechatxt.setText("");
         conPrincipalVw.Ivatxt.setText("");
         conPrincipalVw.Descuentotxt.setText("");
         conPrincipalVw.Proveedortxt.setText("");
    }
     
     public void Mostrarordendecompra(String Valor){
        DefaultTableModel modo= new DefaultTableModel();
        modo.addColumn("ID");
        modo.addColumn("Codigo");
        modo.addColumn("Fecha");
        modo.addColumn("Proveedor");
        modo.addColumn("Producto");
        modo.addColumn("Cantidad");
        modo.addColumn("Precio");
        modo.addColumn("IVA");
        modo.addColumn("Descuento");
        conPrincipalVw.Tabla2.setModel(modo);
        
            String sql;
            if (Valor.equals("")) {
            sql="SELECT* FROM compra";
        }else{
         sql="SELECT* FROM compra WHERE Proveedor='"+Valor+"'";       
    } 
            String datos[]= new String [10];
            try{
            Statement st= cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                datos[5]=rs.getString(6);
                datos[6]=rs.getString(7);
                datos[7]=rs.getString(8);
                datos[8]=rs.getString(9);
                modo.addRow(datos);
                }
            conPrincipalVw.Tabla2.setModel(modo);
            }catch(SQLException ex){
             JOptionPane.showMessageDialog(null, "No se pudo mostar");    
            }
    }
     
    public void Agregarordendecompra(){
        try{
            String Producto= conPrincipalVw.Productotxt.getText();
            String Codigo= conPrincipalVw.Codigotxt.getText();
            String Cantidad= conPrincipalVw.Cantidadtxt.getText();
            String Precio= conPrincipalVw.Preciotxt.getText();
            String Fecha= conPrincipalVw.fechatxt.getText();
            String Iva= conPrincipalVw.Ivatxt.getText();
            String Descuento= conPrincipalVw.Descuentotxt.getText();
            String Proveedor= conPrincipalVw.Proveedortxt.getText();
            
            
            if (Producto.equals("") ||  Codigo.equals("") ||   Cantidad.equals("") || Precio.equals("") || Fecha.equals("") || Iva.equals("") || Descuento.equals("") || Proveedor.equals("")) {
                JOptionPane.showMessageDialog(null, "ingresa valores antes de agregar");
            }else{
            PreparedStatement  ppt= cn.prepareStatement("INSERT INTO compra(Codigo,Fecha,Proveedor,Producto,Cantidad,Precio,IVA,Descuento) VALUES(?,?,?,?,?,?,?,?)");
          ppt.setString(1, conPrincipalVw.Codigotxt.getText());
          ppt.setString(2, conPrincipalVw.fechatxt.getText());
          ppt.setString(3, conPrincipalVw.Proveedortxt.getText());
          ppt.setString(4, conPrincipalVw.Productotxt.getText());
          ppt.setString(5, conPrincipalVw.Cantidadtxt.getText());
          ppt.setString(6, conPrincipalVw.Preciotxt.getText());
          ppt.setString(7, conPrincipalVw.Ivatxt.getText());
          ppt.setString(8, conPrincipalVw.Descuentotxt.getText());
          ppt.executeUpdate();
          JOptionPane.showMessageDialog(null,"Valores agregados con exito");
          Mostrarordendecompra("");
           limpiar1();
        }
             } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null,"No se pudo agregar crack"+ex);
      }         
    }
      
    public void Nuevaordendecompra(){
        limpiar1();
         conPrincipalVw.Productotxt.setEditable(true);
        conPrincipalVw.Codigotxt.setEditable(true);
        conPrincipalVw.Cantidadtxt.setEditable(true);
        conPrincipalVw.Preciotxt.setEditable(true);
        conPrincipalVw.btnAgregar2.setEnabled(true);
        conPrincipalVw.btnModificar2.setEnabled(true);
        conPrincipalVw.btnCancelar2.setEnabled(true);
        conPrincipalVw.fechatxt.setEnabled(true);
         conPrincipalVw.Ivatxt.setEnabled(true);
         conPrincipalVw.Descuentotxt.setEnabled(true);
         conPrincipalVw.Proveedortxt.setEnabled(true);
    }
           public void Bloqueo1(){
        conPrincipalVw.Productotxt.setEditable(false);
        conPrincipalVw.Codigotxt.setEditable(false);
        conPrincipalVw.Cantidadtxt.setEditable(false);
        conPrincipalVw.Preciotxt.setEditable(false);
        conPrincipalVw.btnAgregar2.setEnabled(false);
        conPrincipalVw.btnModificar2.setEnabled(false);
        conPrincipalVw.btnCancelar2.setEnabled(false);
         conPrincipalVw.fechatxt.setEnabled(false);
         conPrincipalVw.Ivatxt.setEnabled(false);
         conPrincipalVw.Descuentotxt.setEnabled(false);
         conPrincipalVw.Proveedortxt.setEnabled(false);
        
        limpiar1();
        
    }
          public void Reporte(){
               try{
              
                   JasperReport Reporte= null;
                   String Patch= "src//Reporte//Proveedores.jasper";
                   Reporte= (JasperReport) JRLoader.loadObjectFromFile(Patch);
                   JasperPrint jprint= JasperFillManager.fillReport(Patch, null,cn);
                   JasperViewer view= new JasperViewer(jprint,false);
                   view.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                   view.setVisible(true);
                    } catch (JRException ex) {
          Logger.getLogger(conPrincipalMdl.class.getName()).log(Level.SEVERE, null, ex);
      }
      
     }    public void ReporteC(){
               try{
              
                   JasperReport Reporte= null;
                   String Patch= "src//Reporte//Orden.jasper";
                   Reporte= (JasperReport) JRLoader.loadObjectFromFile(Patch);
                   JasperPrint jprint= JasperFillManager.fillReport(Patch, null,cn);
                   JasperViewer view= new JasperViewer(jprint,false);
                   view.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                   view.setVisible(true);
                    } catch (JRException ex) {
          Logger.getLogger(conPrincipalMdl.class.getName()).log(Level.SEVERE, null, ex);
      }
      
     }      
  }


