/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eprsoft.model;

import com.erpsoft.view.vista;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class conPrincipalMdl {
     Connection cc;
  Connection cn= Conexion();
  vista V = new vista();
  
  public Connection Conexion(){
      try{
         Class.forName("com.mysql.jdbc.Driver");
         cc= DriverManager.getConnection("jdbc:mysql://localhost/tienda","root","");
         System.out.println("Conexión establecida");
      }catch(Exception e){
        System.out.println(e);  
      }
      return cc;
}
  public void iniciarModelo(){
      Bloqueo();
      MostrarCliente("");
      MostrarPedido("");
      Bloqueo1();
  }
  
 public void EliminarCliente(){
  NuevoCliente();
       vista.btnAgregar.setEnabled(false);
        vista.btnModificar.setEnabled(true);
        int fila= vista.tabla.getSelectedRow();
        if (fila>0) {
            String CODIGO= vista.tabla.getValueAt(fila,0).toString();
            try{
            PreparedStatement ppt= cn.prepareStatement("DELETE FROM cliente  WHERE cod_cli='"+CODIGO+"'");
            ppt.executeUpdate();
            JOptionPane.showConfirmDialog(null, "Seguro que quieres eliminar este Cliente?");
            JOptionPane.showMessageDialog(null, "Se elimino con exito "); 
            MostrarCliente("");
           }catch(SQLException ex){
              JOptionPane.showMessageDialog(null, "Algo hiciste mal");     
           }
        }else{
           JOptionPane.showMessageDialog(null, "Selecciona la fila ");        
        }    
        
  }
 public void ActualizarCliente(){
      try{
          
            String Codigo= vista .buscartxt1.getText();
            String Nombre= vista.nombretxt.getText();
            String Apellido= vista.apellidotxt.getText();
            String Direccion= vista.Direcciontxt.getText();
            String Telefono= vista.telefonotxt.getText();
            String Email= vista.emailtxt.getText();
            String Ruc= vista.Ructxt.getText();
            if (Codigo.equals("") ||  Nombre.equals("") || Apellido.equals("") ||  Direccion.equals("") || Telefono.equals("")|| Email.equals("") || Ruc.equals("") ) {
                JOptionPane.showMessageDialog(null, " ");
            }else{  
              PreparedStatement  ppt= cn.prepareStatement("UPDATE cliente SET "
                      + "cod_cli = '"+Codigo+"',"
                      + "nom_cli='"+Nombre+"',"
                      + "ape_cli='"+Apellido+"',"       
                      + "dir_cli='"+Direccion+"',"
                      + "email_cli= '"+Email+"',"
                      + "tel_cli= '"+Telefono+"',"
                      + "ruc_cli= '"+Ruc+"',"
                      + "WHERE cod_cli='"+Codigo+"'");   
              ppt.executeUpdate();
               JOptionPane.showMessageDialog(null, "Cliente modificado");  
               MostrarCliente("");
               Bloqueo();
            }
        }catch(Exception ex){
       JOptionPane.showMessageDialog(null, "Esta mal "+ex);           
        } 
  }
public void ConsultarCliente(){
      NuevoCliente();
       vista.btnAgregar.setEnabled(false);
        vista.btnModificar.setEnabled(true);
        int fila= vista.tabla.getSelectedRow();
        if (fila>0) {
           vista.buscartxt1.setText(vista.tabla.getValueAt(fila, 0).toString());
           vista.codigotxt.setText(vista.tabla.getValueAt(fila, 1).toString());
           vista.nombretxt.setText(vista.tabla.getValueAt(fila, 2).toString());
           vista.apellidotxt.setText(vista.tabla.getValueAt(fila,3).toString());
           vista.emailtxt.setText(vista.tabla.getValueAt(fila, 4).toString());
           vista.Ructxt.setText(vista.tabla.getValueAt(fila, 5).toString());
           vista.Direcciontxt.setText(vista.tabla.getValueAt(fila, 6).toString());
           vista.telefonotxt.setText(vista.tabla.getValueAt(fila, 7).toString());
           
        }else{
           JOptionPane.showMessageDialog(null, "Selecciona la fila ");        
        }
   
    }
  public static void main(String[] args) {
        conPrincipalMdl m= new conPrincipalMdl();
        m.Conexion();
    }

    
     public void limpiar(){
        vista.codigotxt.setText("");
        vista.nombretxt.setText("");
        vista.apellidotxt.setText("");
        vista.emailtxt.setText("");
        vista.Ructxt.setText("");
        vista.Direcciontxt.setText("");
        vista.telefonotxt.setText("");
        }
     public void MostrarCliente(String Valor){
        DefaultTableModel modo= new DefaultTableModel();
        modo.addColumn("Codigo");
        modo.addColumn("Nombre");
        modo.addColumn("Apellido");
        modo.addColumn("Email");
        modo.addColumn("Ruc");
        modo.addColumn("Direccion");
        modo.addColumn("Telefono");
        vista.tabla.setModel(modo);
        
            String sql;
            if (Valor.equals("")) {
            sql="SELECT* FROM cliente";
        }else{
         sql="SELECT* FROM cliente WHERE cod_cli='"+Valor+"'";       
    } 
            String datos[]= new String [8];
            try{
            Statement st= cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                datos[5]=rs.getString(6);
                datos[6]=rs.getString(7);
                
                modo.addRow(datos);
                }
            vista.tabla.setModel(modo);
            }catch(SQLException ex){
             JOptionPane.showMessageDialog(null, "No se pudo mostrar "+ex);    
            }
            }
    public void AgregarCliente(){
        try{
            String Codigo= vista.codigotxt.getText();
            String Nombre= vista.nombretxt.getText();
            String Apellido= vista.apellidotxt.getText();
            String Email= vista.emailtxt.getText();
            String Ruc= vista.Ructxt.getText();
            String Direccion= vista.Direcciontxt.getText();
            String Telefono= vista.telefonotxt.getText();
            
            if (Codigo.equals("") ||  Nombre.equals("") || Apellido.equals("") ||  Direccion.equals("") || Telefono.equals("")|| Email.equals("") ||Ruc.equals("") ) {
                JOptionPane.showMessageDialog(null, " Ingresa valores antes de agregar");
            }else{
            PreparedStatement  ppt= cn.prepareStatement("INSERT INTO  (cod_cli,nom_cli,ape_cli,tel_cli,email_cli,dir_cli,ruc_cli ) VALUES (?,?,?,?,?,?,?)");
          ppt.setString(1, vista.codigotxt.getText());
          ppt.setString(2, vista.nombretxt.getText());
          ppt.setString(3, vista.apellidotxt.getText());
          ppt.setString(4, vista.emailtxt.getText());
          ppt.setString(1, vista.Ructxt.getText());
          ppt.setString(3, vista.Direcciontxt.getText());
          ppt.setString(4, vista.telefonotxt.getText());
          ppt.executeUpdate();
          JOptionPane.showConfirmDialog(null, "Vas a agregar un nuevo cliente, es correcto?");
         
          JOptionPane.showMessageDialog(null," Agregado ");
             limpiar();
        }
             }catch(SQLException ex){
          JOptionPane.showMessageDialog(null,"No agregado"); 
        }
          
    }
    public void NuevoCliente(){
        limpiar();
        vista.codigotxt.setEditable(true);
        vista.nombretxt.setEditable(true);
        vista.apellidotxt.setEditable(true);
        vista.emailtxt.setEditable(true);
        vista.Ructxt.setEditable(true);
        vista.Direcciontxt.setEditable(true);
        vista.telefonotxt.setEditable(true);
        vista.btnAgregar.setEnabled(true);
        vista.btnModificar.setEnabled(true);
        vista.btnCancelar.setEnabled(true); 
    }
      public void Bloqueo(){
        vista.codigotxt.setEditable(false);
        vista.nombretxt.setEditable(false);
        vista.apellidotxt.setEditable(false);
        vista.emailtxt.setEditable(false);
        vista.Ructxt.setEditable(false);
        vista.Direcciontxt.setEditable(false);
        vista.telefonotxt.setEditable(false);
        vista.btnAgregar.setEnabled(false);
        vista.btnModificar.setEnabled(false);
        vista.btnCancelar.setEnabled(false); 
        
        limpiar();
        
    }
       //codigo para PEDIDO//
    public void EliminarPedido(){
  NuevoPedido();
       vista.btnAgregar2.setEnabled(false);
        vista.btnModificar2.setEnabled(true);
        int fila= vista.Tabla2.getSelectedRow();
        if (fila>0) {
            String Numbol= vista.Tabla2.getValueAt(fila,0).toString();
            try{
            PreparedStatement ppt= cn.prepareStatement("DELETE FROM detalleboleta WHERE num_bol='"+Numbol+"'");
            ppt.executeUpdate();
            JOptionPane.showConfirmDialog(null, "Seguro que quieres eliminar este cliente?");
            JOptionPane.showMessageDialog(null, "Se elimino con exito "); 
            MostrarPedido("");
           }catch(SQLException ex){
              JOptionPane.showMessageDialog(null, "Algo hiciste mal");     
           }
        }else{
           JOptionPane.showMessageDialog(null, "Selecciona la fila ");        
        }    
  }
 public void ActualizarPedido(){
      try{
            String Codigo= vista .buscartxt15.getText();
            String Numero= vista .descripciontxt.getText();
            String Cantidad= vista.cantidadtxt.getText();
            String Unitario= vista.unitariotxt.getText();
            String  Descripcion= vista .descripciontxt.getText();
            String  Total= vista .totaltxt.getText();
            
            
            if ( Numero.equals("") || Codigo.equals("") ||   Descripcion.equals("") ||Cantidad.equals("") || Unitario.equals("") ||Total.equals("")  ) {
                JOptionPane.showMessageDialog(null, " ");
            }else{  
              PreparedStatement  ppt= cn.prepareStatement("UPDATE detalleboleta SET "
                      + "num_bol = '"+Numero+"',"
                      + "cod_pro = '"+Codigo+"',"
                      + "des_pro='"+Descripcion+"',"
                      + "cantidad_pro='"+Cantidad+"',"       
                      + "pre_unit='"+Unitario+"',"
                      + "pre_venta='"+Total+"'"
                      + "WHERE num_bol='"+Codigo+"'");   
              ppt.executeUpdate();
               JOptionPane.showMessageDialog(null, "Pedido modificado");  
               MostrarPedido("");
               Bloqueo();
            }
        }catch(Exception e){
       JOptionPane.showMessageDialog(null, "Esta mal"+e);           
        } 
  }
 public void ConsultarPedido(){
      NuevoPedido();
       vista.btnAgregar2.setEnabled(false);
        vista.btnModificar2.setEnabled(true);
        int fila= vista.Tabla2.getSelectedRow();
        if (fila>0) {
           vista.buscartxt15.setText(vista.Tabla2.getValueAt(fila, 0).toString());
           vista.descripciontxt.setText(vista.Tabla2.getValueAt(fila, 1).toString());
           vista.codigotxt.setText(vista.Tabla2.getValueAt(fila, 2).toString());
           vista.cantidadtxt.setText(vista.Tabla2.getValueAt(fila, 3).toString());
           vista.unitariotxt.setText(vista.Tabla2.getValueAt(fila, 4).toString());
           vista.numerotxt.setText(vista.Tabla2.getValueAt(fila, 5).toString());
           vista.totaltxt.setText(vista.Tabla2.getValueAt(fila, 5).toString());
        }else{
           JOptionPane.showMessageDialog(null, "Selecciona la fila");        
        }
    }
 public void limpiar1(){
        vista.descripciontxt.setText("");
        vista.codigotxt.setText("");
        vista.cantidadtxt.setText("");
        vista.unitariotxt.setText("");
        vista.totaltxt.setText("");
        vista.numerotxt.setText("");
        }
 public void MostrarPedido(String Valor){
        DefaultTableModel modo= new DefaultTableModel();
        modo.addColumn("NUMERO");
        modo.addColumn("CODIGO");
        modo.addColumn("DESCUENTO");
        modo.addColumn("CANTIDAD");
        modo.addColumn("PRECIO");
        modo.addColumn("TOTAL");
        vista.Tabla2.setModel(modo);
        
            String sql;
            if (Valor.equals("")) {
            sql="SELECT* FROM detalleboleta";
        }else{
         sql="SELECT* FROM pedido WHERE num_bol='"+Valor+"'";       
    } 
            String datos[]= new String [8];
            try{
            Statement st= cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                datos[5]=rs.getString(6);
                datos[6]=rs.getString(7);
                modo.addRow(datos);
                }
            vista.Tabla2.setModel(modo);
            }catch(SQLException ex){
             JOptionPane.showMessageDialog(null, "No se pudo mostar");    
            }
    }
         public void AgregarPedido(){
        try{
            String Numero= vista.descripciontxt.getText();
            String Codigo= vista.codigotxt.getText();
            String Cantidad= vista.cantidadtxt.getText();
            String Unitario= vista.unitariotxt.getText();
            String Descripcion= vista.cantidadtxt.getText();
            String Total=vista.totaltxt.getText();
            
            if (Numero.equals("") ||  Codigo.equals("") || Descripcion.equals("") ||   Cantidad.equals("") || Unitario.equals("")|| Total.equals("")) {
                JOptionPane.showMessageDialog(null, "ingresa valores antes de agregar");
            }else{
            PreparedStatement  ppt= cn.prepareStatement("INSERT INTO detalleboleta  (num_bol,cod_pro,des_pro,cant_pro,pre_unit,pre_venta,  ) VALUES (?,?,?,?,?,?)");
          ppt.setString(1, vista.numerotxt.getText());
          ppt.setString(2, vista.codigotxt.getText());
          ppt.setString(3, vista.descripciontxt.getText());
          ppt.setString(4, vista.cantidadtxt.getText());
          ppt.setString(5, vista.unitariotxt.getText());
          ppt.setString(5, vista.totaltxt.getText());
          ppt.executeUpdate();
          JOptionPane.showConfirmDialog(null, "Valores agrgados con exito");
         
          
          MostrarPedido("");
             limpiar();
        }
             } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null,"No se pudo agregar crack"+ex);
      }          
    }
public void NuevoPedido(){
        limpiar1();
         vista.descripciontxt.setEditable(true);
        vista.codigotxt.setEditable(true);
        vista.numerotxt.setEditable(true);
        vista.descripciontxt.setEditable(true);
        vista.cantidadtxt.setEditable(true);
        vista.unitariotxt.setEditable(true);
        vista.totaltxt.setEditable(true);
        vista.btnAgregar2.setEnabled(true);
        vista.btnModificar2.setEnabled(true);
        vista.btnCancelar2.setEnabled(true); 
        
        
         
  }
public void Bloqueo1(){
        
        vista.btnAgregar2.setEnabled(false);
        vista.btnModificar2.setEnabled(false);
        vista.btnCancelar2.setEnabled(false);
       
        
        limpiar1();
        
    }
public void ReporteCliente(){
               try{
              //
                   JasperReport Reporte= null;
                   String Patch= "src//Reporte/Cliente.jasper";
                   Reporte= (JasperReport) JRLoader.loadObjectFromFile(Patch);
                   JasperPrint jprint= JasperFillManager.fillReport(Patch, null,cn);
                   JasperViewer view= new JasperViewer(jprint,false);
                   view.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                   view.setVisible(true);
                    } catch (JRException ex) {
          Logger.getLogger(conPrincipalMdl.class.getName()).log(Level.SEVERE, null, ex);
      }
        }public void ReportePedido(){
               try{
              
                   JasperReport Reporte= null;
                   String Patch= "src//Reporte//Pedido.jasper";
                   Reporte= (JasperReport) JRLoader.loadObjectFromFile(Patch);
                   JasperPrint jprint= JasperFillManager.fillReport(Patch, null,cn);
                   JasperViewer view= new JasperViewer(jprint,false);
                   view.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
                   view.setVisible(true);
                    } catch (JRException ex) {
          Logger.getLogger(conPrincipalMdl.class.getName()).log(Level.SEVERE, null, ex);
      }
               
               
       }
        
        
  }   