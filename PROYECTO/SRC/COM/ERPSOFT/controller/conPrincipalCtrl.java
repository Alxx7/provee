
package com.erpsoft.controller;
import com.eprsoft.model.conPrincipalMdl;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.erpsoft.view.vista;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class conPrincipalCtrl implements ActionListener{
    private final conPrincipalMdl m;    
    private final vista v;
    
 public conPrincipalCtrl(conPrincipalMdl m, vista v) {
        this.m = m;
        this.v = v;
        
        vista.btnNuevo.addActionListener(this);
        vista.btnCancelar.addActionListener(this);
        vista.btnAgregar.addActionListener(this);
        vista.btnRefrescar.addActionListener(this);
        vista.btnBuscar.addActionListener(this);
        vista.btnModificar.addActionListener(this);
        vista.btnActualizar.addActionListener(this);
        vista.btnEliminar.addActionListener(this);
        vista.btnNuevo2.addActionListener(this);
        vista.btnCancelar2.addActionListener(this);
        vista.btnAgregar2.addActionListener(this);
        vista.btnRefrescar2.addActionListener(this);
        vista.btnBuscar2.addActionListener(this);
        vista.btnModificar2.addActionListener(this);
        vista.btnActualizar2.addActionListener(this);
        vista.btnEliminar2.addActionListener(this);
        vista.btnreporte.addActionListener(this);
        vista.btnreporte2.addActionListener(this);
    }
 

public void iniciar(){
        v.setTitle("cliente");
        v.pack();
        v.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        v.setLocationRelativeTo(null);
        v.setVisible(true);
        m.iniciarModelo();
        
       
        
    }
@Override
      public void actionPerformed(ActionEvent e){
        if (vista.btnNuevo== e.getSource()) {
            try{
              m.NuevoCliente();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo agregar");
            }
        }else if (vista.btnCancelar== e.getSource()) {
            try{
              m.Bloqueo();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo Cancelar");  
            }
        }else if (vista.btnAgregar== e.getSource()) {
            try{
              m.AgregarCliente();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo AGREGAR ");  
            }
        }else if (vista.btnRefrescar== e.getSource()) {
            try{
              m.MostrarCliente("");
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo refrescar la tabla ");  
            }
        }else if (vista.btnBuscar== e.getSource()) {
            try{
              m.MostrarCliente(vista.buscartxt1.getText());
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se encontro ese Cliente ");  
            }
        }else if (vista.btnModificar== e.getSource()) {
            try{
              m.ConsultarCliente();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No pudimos consultar  ");  
            }
        }else if (vista.btnActualizar== e.getSource()) {
            try{
              m.ActualizarCliente();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo actualizar el cliente  ");  
            }
        }else if (vista.btnEliminar== e.getSource()) {
            try{
              m.EliminarCliente();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo eliminar ");  
            }
        }
        //codigo Pedido
 
        if (vista.btnNuevo2== e.getSource()) {
            try{
              m.NuevoPedido();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo agregar");
            }
        }else if (vista.btnCancelar2== e.getSource()) {
            try{
              m.Bloqueo();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo Cancelar");  
            }
        }else if (vista.btnAgregar2== e.getSource()) {
            try{
              m.AgregarPedido();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo AGREGAR ");  
            }
        }else if (vista.btnRefrescar2== e.getSource()) {
            try{
              m.MostrarPedido("");
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo refrescar la tabla ");  
            }
        }else if (vista.btnBuscar2== e.getSource()) {
            try{
              m.MostrarPedido(vista.buscartxt15.getText());
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se encontro pedido ");  
            }
        }else if (vista.btnModificar2== e.getSource()) {
            try{
              m.ConsultarPedido();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No pudimos consultar  ");  
            }
        }else if (vista.btnActualizar2== e.getSource()) {
            try{
              m.ActualizarPedido();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo actualizar orden de compra  ");  
            }
        }else if (vista.btnEliminar2== e.getSource()) {
            try{
              m.EliminarPedido();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo eliminar ");  
            }
        }else if (vista.btnreporte== e.getSource()) {
            try{
              m.ReporteCliente();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo mostrar el reporte ");  
            }
        }else if (vista.btnreporte2== e.getSource()) {
            try{
              m.ReportePedido();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo mostrar el reporte de compra ");  
            }
        }
    } 
}
