
package SoftwareERP5tob;
import com.eprsoft.model.conPrincipalMdl;
import com.erpsoft.view.vista;
import com.erpsoft.controller.conPrincipalCtrl;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
public class Softwareerp5tob {

   
    public static void main(String[] args) {
      
        try{
           UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    }
        conPrincipalMdl m= new conPrincipalMdl();
        vista v= new vista();
        conPrincipalCtrl c= new conPrincipalCtrl(m,v);
        c.iniciar();
    }
    
}
