
package com.erpsoft.controller;

import com.erpsoft.view.conPrincipalVw;
import com.erpsoft.model.conPrincipalMdl;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;



public class conPrincipalCtrl implements ActionListener {
  
    private conPrincipalMdl modC;
    private conPrincipalVw frm;
    
    public conPrincipalCtrl(conPrincipalMdl modC, conPrincipalVw frm){
       
        this.modC= modC;
        this.frm= frm;
        this.frm.btnbuscar.addActionListener(this);
        this.frm.btnguardar.addActionListener(this);
        this.frm.btnrefrescar.addActionListener(this);
        this.frm.btnmodificar.addActionListener(this);
        this.frm.btnactua.addActionListener(this);
        this.frm.btnlimpiar.addActionListener(this);
        this.frm.btneliminar.addActionListener(this);
        this.frm.btnReporte.addActionListener(this);
        this.frm.btnEnlazar.addActionListener(this);
        
        //Botones de equipo
        this.frm.btnGuardar2.addActionListener(this);
        this.frm.btnEliminar.addActionListener(this);
        this.frm.btnActualizar.addActionListener(this);
        this.frm.btnLimpiar.addActionListener(this);
        this.frm.btnModificar.addActionListener(this);
        this.frm.btnBuscar2.addActionListener(this);
        
    }
    
    public void iniciar(){
            frm.setTitle("Sistema MVC");
            frm.setVisible(true);
            frm.setLocationRelativeTo(null);
            modC.IniciarModelo();
            
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(frm.btnguardar == e.getSource()){
        try{
            modC.InsertarUsuario();
            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede guardar");
            }           
        }
        else if(frm.btnrefrescar == e.getSource()){
            try{
            modC.mostrarusuario("");
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede mostrar tabla");
            }           
        }
        else if(frm.btnbuscar == e.getSource()){
            try{
            modC.mostrarusuario(frm.txtbuscar.getText());
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede buscar en la tabla");
            }           
        }
        else if(frm.btnmodificar== e.getSource()){
            try{
            modC.consultarusuario();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede modificar");
            }           
        }
        else if(frm.btnlimpiar== e.getSource()){
            try{
            modC.limpiar();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede limpiar los datos");
            }           
        }
        else if(frm.btnactua== e.getSource()){
            try{
            modC.actualizarusuarios();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede actualizar los datos");
            }           
        }
        else if(frm.btneliminar== e.getSource()){
            try{
            modC.eliminarusuario();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede eliminar los datos");
            }           
        }
        
    //Accion de botones equipo
        if(frm.btnGuardar2 == e.getSource()){
        try{
            modC.InsertarEquipo();
            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede guardar");
            }           
        }
        else if(frm.btnEliminar== e.getSource()){
            try{
            modC.eliminarequipo();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede eliminar los datos");
            }           
        }
        else if(frm.btnActualizar== e.getSource()){
            try{
            modC.actualizarequipo();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede actualizar los datos");
            }           
        }
        else if(frm.btnLimpiar== e.getSource()){
            try{
            modC.limpiar2();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede limpiar los datos");
            }           
        }
        else if(frm.btnModificar== e.getSource()){
            try{
            modC.consultarequipo();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede modificar");
            }           
        }
        else if(frm.btnBuscar2 == e.getSource()){
            try{
            modC.mostrarequipo(frm.txtBusca.getText());
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede buscar en la tabla");
            }           
        }
        else if(frm.btnReporte == e.getSource()){
            try{
            modC.Reporte();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede buscar en la tabla");
            }           
        }
        
        if(e.getSource() == frm.btnEnlazar){
            modC.setCodcliente(frm.txtcliente.getText());
            if(modC.buscar(modC)){
                frm.txtnombre.setText(modC.getNombre());
                frm.txtapellido.setText(modC.getApellido());
                frm.txtfono.setText(modC.getTelefono());
                frm.txtcedula.setText(modC.getCedula());
                frm.txtcorreo.setText(modC.getCorreo());
                frm.txtdireccion.setText(modC.getDireccion());
                frm.txtruta.setText(modC.getRuta());
            }else{
                JOptionPane.showMessageDialog(null, "No se puede encontrar resultados");
            }
            
        }
    }
}
