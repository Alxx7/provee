/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erpsoft.model;

import com.erpsoft.view.conPrincipalVw;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;





public class conPrincipalMdl {
    Connection cc;
    Connection cn= Conexion();
    conPrincipalVw frm = new conPrincipalVw();
    
    public Connection Conexion(){
        try{
                Class.forName("com.mysql.jdbc.Driver");
               cc= DriverManager.getConnection("jdbc:mysql://sql10.freesqldatabase.com:3306/sql10283376","sql10283376","nC8rpJ951Z");
               //cc= DriverManager.getConnection("jdbc:mysql://localhost:3306/distribucion","root","");
                System.out.println("Conexion exitosa");
            }catch(ClassNotFoundException | SQLException e){
                System.err.println(e);
            }
            return cc;
        }
    
    public void IniciarModelo(){
        mostrarusuario("");
        mostrarequipo("");
    }
    
    public void consultarusuario(){
        int fila = frm.Datos.getSelectedRow();
        if(fila>=0){
            frm.txtbuscar.setText(frm.Datos.getValueAt(fila, 0).toString());
            frm.txtcodruta.setText(frm.Datos.getValueAt(fila, 1).toString());
            frm.txtciudad.setText(frm.Datos.getValueAt(fila, 2).toString());
            frm.txtcanton.setText(frm.Datos.getValueAt(fila, 3).toString());
            frm.txtparroquia.setText(frm.Datos.getValueAt(fila, 4).toString());
            frm.txtzona.setText(frm.Datos.getValueAt(fila, 5).toString());
            frm.txtdestino.setText(frm.Datos.getValueAt(fila, 6).toString());
            frm.txtreferencia.setText(frm.Datos.getValueAt(fila, 7).toString());
            frm.txtobservacion.setText(frm.Datos.getValueAt(fila, 8).toString());
        }else{
            JOptionPane.showMessageDialog(null,"Fila no seleccionada");
        }
    }
    
    public void actualizarusuarios(){
        try{
            String id = frm.txtbuscar.getText();
            String codruta = frm.txtcodruta.getText();
            String ciudad = frm.txtciudad.getText();
            String canton = frm.txtcanton.getText();
            String parroquia = frm.txtparroquia.getText();
            String zona = frm.txtzona.getText();
            String destino = frm.txtdestino.getText();
            String referencia = frm.txtreferencia.getText();
            String observacion = frm.txtobservacion.getText();
            if(codruta.equals("") || ciudad.equals("") || canton.equals("") || parroquia.equals("") || zona.equals("") || destino.equals("") || referencia.equals("") || observacion.equals(""))
            {
            JOptionPane.showMessageDialog(null, "ERROR, Campos vacios");
            }else{
                PreparedStatement ppt = cn.prepareStatement("UPDATE ruta_de_entrega SET "
                        + "codruta='"+codruta+"',"
                        + "ciudad='"+ciudad+"',"
                        + "canton='"+canton+"',"
                        + "parroquia='"+parroquia+"',"
                        + "zona='"+zona+"',"
                        + "destino='"+destino+"',"
                        + "referencia='"+referencia+"',"
                        + "observacion='"+observacion+"'"
                        + "WHERE id='"+id+"'");
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Datos modificados");
                mostrarusuario("");
                limpiar();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    
    public void eliminarusuario(){
        int fila = frm.Datos.getSelectedRow();
        if(fila>=0){
            String id=frm.Datos.getValueAt(fila, 0).toString();
            try{
            PreparedStatement ppe = cn.prepareStatement("DELETE FROM ruta_de_entrega WHERE id='"+id+"'");
            ppe.executeUpdate();
            JOptionPane.showMessageDialog(null,"Datos eliminiados");
            mostrarusuario("");
            
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(null,"No se puede eliminar");
            }
        }else{
            JOptionPane.showMessageDialog(null,"Fila no seleccionada");
        }
    }
    
    public void limpiar(){
        frm.txtbuscar.setText("");
        frm.txtcodruta.setText("");
        frm.txtciudad.setText("");
        frm.txtcanton.setText("");
        frm.txtparroquia.setText("");
        frm.txtzona.setText("");
        frm.txtdestino.setText("");
        frm.txtreferencia.setText("");
        frm.txtobservacion.setText("");
    }
    
    public void mostrarusuario(String atributo){
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Id");
        modo.addColumn("Codigo de ruta");
        modo.addColumn("Ciudad");
        modo.addColumn("Canton");
        modo.addColumn("Parroquia");
        modo.addColumn("Zona");
        modo.addColumn("Destino");
        modo.addColumn("Referencia");
        modo.addColumn("Observacion");
        frm.Datos.setModel(modo);
        
        String sql="SELECT * FROM ruta_de_entrega";
        
        if(atributo.equals("")){
            sql="SELECT * FROM ruta_de_entrega";
        }else{
            sql="SELECT * FROM ruta_de_entrega WHERE id='"+atributo+"'";
        }
        
        String datos[] = new String[9];
        try{
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                datos[5]=rs.getString(6);
                datos[6]=rs.getString(7);
                datos[7]=rs.getString(8);
                datos[8]=rs.getString(9);
                modo.addRow(datos);
            }
            frm.Datos.setModel(modo);
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "No se pudo mostrar la informacion");
        }       
    }
    
    public void InsertarUsuario(){
        try{
            String codruta = frm.txtcodruta.getText();
            String ciudad = frm.txtciudad.getText();
            String canton = frm.txtcanton.getText();
            String parroquia = frm.txtparroquia.getText();
            String zona = frm.txtzona.getText();
            String destino = frm.txtdestino.getText();
            String referencia = frm.txtreferencia.getText();
            String observacion = frm.txtobservacion.getText();
            if(codruta.equals("") || ciudad.equals("") || canton.equals("") || parroquia.equals("") || zona.equals("") || destino.equals("") || referencia.equals("") || observacion.equals(""))
            {
            JOptionPane.showMessageDialog(null, "ERROR, Campos vacios");
            }else{
            PreparedStatement pps = cn.prepareStatement("INSERT INTO ruta_de_entrega (codruta,ciudad,canton,parroquia,zona,destino,referencia,observacion) values(?,?,?,?,?,?,?,?)");
            pps.setString(1, frm.txtcodruta.getText());
            pps.setString(2, frm.txtciudad.getText());
            pps.setString(3, frm.txtcanton.getText());
            pps.setString(4, frm.txtparroquia.getText());
            pps.setString(5, frm.txtzona.getText());
            pps.setString(6, frm.txtdestino.getText());
            pps.setString(7, frm.txtreferencia.getText());
            pps.setString(8, frm.txtobservacion.getText());
            pps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Datos guardados");
            mostrarusuario("");
            limpiar();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al guardar");
        }
    }
    
    
    
    //INICIA CODIGO DE EQUIPO
    
    public void consultarequipo(){
        int fila = frm.Equipo.getSelectedRow();
        if(fila>=0){
            frm.txtBusca.setText(frm.Equipo.getValueAt(fila, 0).toString());
            frm.txtCodRuta3.setText(frm.Equipo.getValueAt(fila, 1).toString());            
            frm.txtChofer.setText(frm.Equipo.getValueAt(fila, 2).toString());
            frm.txtAsis1.setText(frm.Equipo.getValueAt(fila, 3).toString());
            frm.txtAsis2.setText(frm.Equipo.getValueAt(fila, 4).toString());
            frm.txtCobrador.setText(frm.Equipo.getValueAt(fila, 5).toString());
            frm.txtMatricula.setText(frm.Equipo.getValueAt(fila, 6).toString());
            frm.txtTipo.setText(frm.Equipo.getValueAt(fila, 7).toString());
            frm.txtColor.setText(frm.Equipo.getValueAt(fila, 8).toString());
            frm.txtObser.setText(frm.Equipo.getValueAt(fila, 9).toString());
        }else{
            JOptionPane.showMessageDialog(null,"Fila no seleccionada");
        }
    }
        
    public void InsertarEquipo(){
        try{
            String codruta3 = frm.txtCodRuta3.getText();
            String Chofer = frm.txtChofer.getText();
            String Asis1 = frm.txtAsis1.getText();
            String Asis2 = frm.txtAsis2.getText();
            String Cobrador = frm.txtCobrador.getText();
            String Matricula = frm.txtMatricula.getText();
            String Tipo = frm.txtTipo.getText();
            String Color = frm.txtColor.getText();
            String Obser = frm.txtObser.getText();
            if(codruta3.equals("") || Chofer.equals("") || Asis1.equals("") || Asis2.equals("") || Cobrador.equals("") || Matricula.equals("") || Tipo.equals("") || Color.equals("") || Obser.equals(""))
            {
            JOptionPane.showMessageDialog(null, "ERROR, Campos vacios");
            }else{
            PreparedStatement pps = cn.prepareStatement("INSERT INTO equipo (codruta,chofer,asistente1,asistente2,cobrador,matricula,tipodecarro,color,observacion) values(?,?,?,?,?,?,?,?,?)");
            pps.setString(1, frm.txtCodRuta3.getText());
            pps.setString(2, frm.txtChofer.getText());
            pps.setString(3, frm.txtAsis1.getText());
            pps.setString(4, frm.txtAsis2.getText());
            pps.setString(5, frm.txtCobrador.getText());
            pps.setString(6, frm.txtMatricula.getText());
            pps.setString(7, frm.txtTipo.getText());
            pps.setString(8, frm.txtColor.getText());
            pps.setString(9, frm.txtObser.getText());
            pps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Datos guardados");
            mostrarequipo("");
            limpiar2();
            
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al guardar");
        }
    }
    
    public void mostrarequipo(String atributo){
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Codigo de equipo");
        modo.addColumn("Codigo de ruta");
        modo.addColumn("Chofer");
        modo.addColumn("Asistente1");
        modo.addColumn("Asistente2");
        modo.addColumn("Cobrador");
        modo.addColumn("Matricula");
        modo.addColumn("Tipo de vehiculo");
        modo.addColumn("Color");
        modo.addColumn("Observacion");
        frm.Equipo.setModel(modo);
        
        String sql="SELECT * FROM equipo";
        
        if(atributo.equals("")){
            sql="SELECT * FROM equipo";
        }else{
            sql="SELECT * FROM equipo WHERE codequipo='"+atributo+"'";
        }
        
        String datos[] = new String[10];
        try{
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                datos[5]=rs.getString(6);
                datos[6]=rs.getString(7);
                datos[7]=rs.getString(8);
                datos[8]=rs.getString(9);
                datos[9]=rs.getString(10);
                modo.addRow(datos);
            }
            frm.Equipo.setModel(modo);
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "No se pudo mostrar la informacion");
        }       
    }

    public void eliminarequipo(){
        int fila = frm.Equipo.getSelectedRow();
        if(fila>=0){
            String id=frm.Equipo.getValueAt(fila, 0).toString();
            try{
            PreparedStatement ppe = cn.prepareStatement("DELETE FROM equipo WHERE codequipo='"+id+"'");
            ppe.executeUpdate();
            JOptionPane.showMessageDialog(null,"Datos eliminiados");
            mostrarequipo("");
            
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(null,"No se puede eliminar");
            }
        }else{
            JOptionPane.showMessageDialog(null,"Fila no seleccionada");
        }
    }

    public void actualizarequipo(){
        try{
            String id = frm.txtBusca.getText();
            String codruta3 = frm.txtCodRuta3.getText();
            String Chofer = frm.txtChofer.getText();
            String Asis1 = frm.txtAsis1.getText();
            String Asis2 = frm.txtAsis2.getText();
            String Cobrador = frm.txtCobrador.getText();
            String Matricula = frm.txtMatricula.getText();
            String Tipo = frm.txtTipo.getText();
            String Color = frm.txtColor.getText();
            String Obser = frm.txtObser.getText();
            if(codruta3.equals("") || Chofer.equals("") || Asis1.equals("") || Asis2.equals("") || Cobrador.equals("") || Matricula.equals("") || Tipo.equals("") || Color.equals("") || Obser.equals(""))
            {
            JOptionPane.showMessageDialog(null, "ERROR, Campos vacios");
            }else{
                PreparedStatement ppt = cn.prepareStatement("UPDATE equipo SET "
                        + "codruta='"+codruta3+"',"
                        + "chofer='"+Chofer+"',"
                        + "asistente1='"+Asis1+"',"
                        + "asistente2='"+Asis2+"',"
                        + "cobrador='"+Cobrador+"',"
                        + "matricula='"+Matricula+"',"
                        + "tipodecarro='"+Tipo+"',"
                        + "color='"+Color+"',"
                        + "observacion='"+Obser+"'"
                        + "WHERE codequipo='"+id+"'");
                ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Datos modificados");
                mostrarequipo("");
                limpiar2();
            }
        }catch(SQLException e){
            JOptionPane.showMessageDialog(null, "Error: "+e);
        }
    }
    
    public void limpiar2(){
        frm.txtBusca.setText("");
        frm.txtCodRuta3.setText("");
        frm.txtChofer.setText("");
        frm.txtAsis1.setText("");
        frm.txtAsis2.setText("");
        frm.txtCobrador.setText("");
        frm.txtMatricula.setText("");
        frm.txtTipo.setText("");
        frm.txtColor.setText("");
        frm.txtObser.setText("");
    }
    
    public void Reporte(){
        try {
            JasperReport reporte =null;
            String path = "src//com//erpsoft//model//Rutas.jasper";
            reporte= (JasperReport) JRLoader.loadObjectFromFile(path);
            JasperPrint jprint = JasperFillManager.fillReport(path, null,cn);
            JasperViewer view = new JasperViewer(jprint, false);
            view.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            view.setVisible(true);
            
        } catch (JRException ex) {
            Logger.getLogger(conPrincipalMdl.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private String codcliente;
    private String nombre;
    private String apellido;
    private String cedula;
    private String telefono;
    private String direccion;
    private String correo;
    private String ruta;

    public String getRuta() {
        return ruta;
    }

    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
    
    public String getCodcliente() {
        return codcliente;
    }

    public void setCodcliente(String codcliente) {
        this.codcliente = codcliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    public boolean buscar(conPrincipalMdl dis){
        PreparedStatement ps = null;
        ResultSet rs=null;
        
        
        String sql = "SELECT * FROM cliente WHERE cod_clie=?";
        try{
            ps = cn.prepareStatement(sql);
            ps.setString(1, dis.getCodcliente());
            rs= ps.executeQuery();
            if(rs.next()){
                 
                dis.setNombre(rs.getString("nom_clie"));                
                dis.setApellido(rs.getString("ape_cli"));
                dis.setCedula(rs.getString("ruc_cli"));
                dis.setTelefono(rs.getString("tel_cli"));
                dis.setDireccion(rs.getString("dir_cli"));
                dis.setCorreo(rs.getString("email_cli"));
                dis.setRuta(rs.getString("cod_ruta"));
                return true;
            }
            
            return false;
        } catch (SQLException ex) {
            System.err.println(ex);
            return false;
        } finally{
        }
    }
    
}