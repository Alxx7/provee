/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.erpsoft.Model;

import com.erp.soft.view.conPrincipalVw;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JOptionPane;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;

import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;






public class conPrincipalModel {
    Connection cc;
    Connection cn= Conexion();
    conPrincipalVw frm = new conPrincipalVw();
    
    public Connection Conexion(){
        try{
                Class.forName("com.mysql.jdbc.Driver");
                cc= DriverManager.getConnection("jdbc:mysql://localhost:3306/crm","root","");
                System.out.println("Conexion exitosa");
            }catch(ClassNotFoundException | SQLException e){
                System.err.println(e);
            }
            return cc;
        }
 
    
    public void IniciarModelo(){
        mostrarusuario("");
        mostrartarea(""); 
    }



    
    public void consultarusuario(){
        int fila = frm.Tabla.getSelectedRow();
        if(fila>=0){
            frm.txtBuscar.setText(frm.Tabla.getValueAt(fila, 0).toString());
            frm.txtId_usuario.setText(frm.Tabla.getValueAt(fila, 1).toString());
            frm.txtNombre.setText(frm.Tabla.getValueAt(fila, 2).toString());
            frm.txtapellido.setText(frm.Tabla.getValueAt(fila, 3).toString());
            frm.txtTelefono.setText(frm.Tabla.getValueAt(fila, 4).toString());
            frm.txtCorreo.setText(frm.Tabla.getValueAt(fila, 5).toString());
            frm.txtdireccion.setText(frm.Tabla.getValueAt(fila, 6).toString());
            
        }else{
            JOptionPane.showMessageDialog(null,"Fila no seleccionada");
        }
    }
    
    public void actualizarusuarios(){
        try{
            String Id = frm.txtBuscar.getText();
           String codusua = frm.txtId_usuario.getText();
            String nombre = frm.txtNombre.getText();
             String apellido = frm.txtapellido.getText();
             String telef = frm.txtTelefono.getText();
            String correo = frm.txtCorreo.getText();                   
            String direccion = frm.txtdireccion.getText();
            
            if(codusua.equals("") || nombre.equals("") || apellido.equals("") || telef.equals("")  || correo.equals("") || direccion.equals(""))
            
           
            {
            JOptionPane.showMessageDialog(null, "ERROR, Campos vacios");
            }else{
                PreparedStatement ppt = cn.prepareStatement("UPDATE usuario SET " 
                        +"id_usuario='"+codusua+"',"
                        +"nombre='"+nombre+"',"
                        +"apellido='"+apellido+"',"
                        +"telefono='"+telef+"',"        
                        +"correo='"+correo+"',"
                        +"direccion='"+direccion+"'"                       
                        +"WHERE id='"+Id+"'");
               ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Datos modificados");
                mostrarusuario("");
                 limpiar();
                     /*PreparedStatement ppt = cn.prepareStatement("UPDATE usuario SET Id_usuario=?, Nombre=?, Correo=?, Telefono=? WHERE Id=? ");
                        ppt.executeUpdate();
                    JOptionPane.showMessageDialog(null, "Datos modificados");
                mostrarusuario("");
                limpiar();*/
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"No se pudo actualizar");
        }
    }
    
    public void eliminarusuario(){
        int fila = frm.Tabla.getSelectedRow();
        if(fila>=0){
            String id=frm.Tabla.getValueAt(fila, 0).toString();
            try{
            PreparedStatement ppe = cn.prepareStatement("DELETE FROM usuario WHERE id='"+id+"'");
            ppe.executeUpdate();
            JOptionPane.showMessageDialog(null,"Datos eliminiados");
            mostrarusuario("");
            
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(null,"No se puede eliminar");
            }
        }else{
            JOptionPane.showMessageDialog(null,"Fila no seleccionada");
        }
    }
    
    public void limpiar(){
        frm.txtId_usuario.setText("");
        frm.txtBuscar.setText("");
        frm.txtNombre.setText("");
        frm.txtapellido.setText("");
        frm.txtTelefono.setText("");        
        frm.txtCorreo.setText("");
        frm.txtdireccion.setText("");
    }
    
    public void mostrarusuario(String atributo){
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("Id");
        modo.addColumn("Id_suario");
        modo.addColumn("Nombre");
        modo.addColumn("Apellido");
        modo.addColumn("Telefono");
        modo.addColumn("Correo");
        modo.addColumn("Direccion");
       
        frm.Tabla.setModel(modo);
        
        String sql="SELECT * FROM usuario";
        
        if(atributo.equals("")){
            sql="SELECT * FROM usuario";
        }else{
            sql="SELECT * FROM usuario WHERE Id_usuario='"+atributo+"'";
        }
        
        String datos[] = new String[7];
        try{
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                datos[0]=rs.getString(1);
                datos[1]=rs.getString(2);
                datos[2]=rs.getString(3);
                datos[3]=rs.getString(4);
                datos[4]=rs.getString(5);
                datos[5]=rs.getString(6);
                datos[6]=rs.getString(7);
                modo.addRow(datos);
            }
            frm.Tabla.setModel(modo);
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "No se pudo mostrar la informacion");
        }       
    }
    
    public void InsertarUsuario(){
        try{
            String codusua = frm.txtId_usuario.getText();
            String nombre = frm.txtNombre.getText();
            String apellido  = frm.txtapellido.getText();
            String telef = frm.txtTelefono.getText();
             String correo = frm.txtCorreo.getText();
            String direccion = frm.txtdireccion.getText();
            if(codusua.equals("") || nombre.equals("") || apellido.equals("") || telef.equals("") || correo.equals("") || direccion.equals(""))
            {
            JOptionPane.showMessageDialog(null, "ERROR, Campos vacios");
            }else{
            PreparedStatement pps = cn.prepareStatement("INSERT INTO usuario (id_usuario,nombre,apellido,telefono,correo,direccion) values(?,?,?,?,?,?)");
            pps.setString(1, frm.txtId_usuario.getText());
            pps.setString(2, frm.txtNombre.getText());
            pps.setString(3, frm.txtapellido.getText());
            pps.setString(4, frm.txtTelefono.getText());
            pps.setString(5, frm.txtCorreo.getText());
            pps.setString(6, frm.txtdireccion.getText());
            
            pps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Usuario guardada");
            mostrarusuario("");
            limpiar();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al guardar");
        }
    }

    
    /*DATOS DE TAREA*/
    
     public void consultartarea(){
        int fila = frm.TablaT.getSelectedRow();
        if(fila>=0){
            frm.txtBuscarT.setText(frm.TablaT.getValueAt(fila, 0).toString());
            frm.txtfechaentrega.setText(frm.TablaT.getValueAt(fila, 1).toString());
            frm.txtNumero_tarea.setText(frm.TablaT.getValueAt(fila, 2).toString());
            frm.txtnombretarea.setText(frm.TablaT.getValueAt(fila, 3).toString());
            frm.txtasunto.setText(frm.TablaT.getValueAt(fila, 4).toString());
             frm.txtpersonaresponsable.setText(frm.TablaT.getValueAt(fila, 5).toString());
            frm.txtcreado.setText(frm.TablaT.getValueAt(fila, 6).toString());
            
        }else{
            JOptionPane.showMessageDialog(null,"Fila no seleccionada");
        }
    }
   
   
    public void InsertarTarea(){
        try{
            String  fecha_entrega = frm.txtfechaentrega.getText();
            String numero_tarea = frm.txtNumero_tarea.getText();
             String nombre_tarea = frm.txtnombretarea.getText();            
             String asunto = frm.txtasunto.getText();
             String persona_responsable = frm.txtpersonaresponsable.getText();
            String creador = frm.txtcreado.getText();
            

            
            
           if(fecha_entrega.equals("") || numero_tarea.equals("") || nombre_tarea.equals("") || asunto.equals("") || persona_responsable.equals("") || creador.equals(""))
            {
            JOptionPane.showMessageDialog(null, "ERROR, Campos vacios");
            }else{
            PreparedStatement pps = cn.prepareStatement("INSERT INTO tarea (fecha_entrega,numero_tarea,nombre_tarea,asunto,persona_responsable,creador) values(?,?,?,?,?,?)");
            pps.setString(1, frm.txtfechaentrega.getText());
            pps.setString(2, frm.txtNumero_tarea.getText());
            pps.setString(3, frm.txtnombretarea.getText());
            pps.setString(4, frm.txtasunto.getText());
            pps.setString(5, frm.txtpersonaresponsable.getText());
            pps.setString(6, frm.txtcreado.getText());
            pps.executeUpdate();
            JOptionPane.showMessageDialog(null, "Datos guardados");
            mostrartarea("");
            limpiarT();
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error al guardar");
        }
    }
    
    
   public void mostrartarea(String atributot){
        DefaultTableModel modo = new DefaultTableModel();
        modo.addColumn("id");
        modo.addColumn("Fecha_Entrega");       
        modo.addColumn("Numero_Tarea");
        modo.addColumn("Nombre_Tarea");
        modo.addColumn("Asunto");
        modo.addColumn("Persona_Responsable");
       modo.addColumn("Creador");
        
        frm.TablaT.setModel(modo);
        
        String sql="SELECT * FROM tarea";
        
        if(atributot.equals("")){
            sql="SELECT * FROM tarea";
        }else{
            sql="SELECT * FROM tarea WHERE Numero_Tarea='"+atributot+"'";
        }
        
        String datost[] = new String[7];
        try{
            Statement stt = cn.createStatement();
            ResultSet rl = stt.executeQuery(sql);
            while(rl.next()){
                datost[0]=rl.getString(1);
                datost[1]=rl.getString(2);
                datost[2]=rl.getString(3);
                datost[3]=rl.getString(4);
                datost[4]=rl.getString(5);
                datost[5]=rl.getString(6);
                datost[6]=rl.getString(7);
                modo.addRow(datost);
            }
            frm.TablaT.setModel(modo);
        }catch(SQLException ex){
            JOptionPane.showMessageDialog(null, "No se pudo mostrar la informacion");
        }       
    }

   
   
  
   
   
    public void actualizartarea(){
        try{
           String Id = frm.txtBuscarT.getText();
           String fecha_entrega = frm.txtfechaentrega.getText();
           String numero_tarea = frm.txtNumero_tarea.getText();
           String nombre_tarea = frm.txtnombretarea.getText();
           String asunto = frm.txtasunto.getText();
           String persona_responsable = frm.txtpersonaresponsable.getText();           
           String creador = frm.txtcreado.getText();
            
             
           
            
            if(fecha_entrega.equals("") || numero_tarea.equals("") || nombre_tarea.equals("") || asunto.equals("") || persona_responsable.equals("") || creador.equals(""))
            
           
            {
            JOptionPane.showMessageDialog(null, "ERROR, Campos vacios");
            }else{
                PreparedStatement ppt = cn.prepareStatement("UPDATE tarea SET " 
                        +"fecha_entrega='"+fecha_entrega+"',"
                        +"numero_tarea='"+numero_tarea+"'," 
                        +"nombre_tarea='"+nombre_tarea+"',"
                        +"asunto='"+asunto+"',"
                        +"persona_responsable='"+persona_responsable+"',"                           
                        +"creador='"+creador+"'"            
                        +"WHERE id='"+Id+"'");
               ppt.executeUpdate();
                JOptionPane.showMessageDialog(null, "Datos modificados");
                mostrartarea("");
                 limpiarT();
                     
            }
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null,"No se pudo actualizar");
        }
    }
   
   
   
   public void eliminartarea(){
        int fila = frm.TablaT.getSelectedRow();
        if(fila>=0){
            String id=frm.TablaT.getValueAt(fila, 0).toString();
            try{
            PreparedStatement ppe = cn.prepareStatement("DELETE FROM tarea WHERE Id='"+id+"'");
            ppe.executeUpdate();
            JOptionPane.showMessageDialog(null,"Datos eliminiados");
            mostrartarea("");
          
            }catch(SQLException ex){
                JOptionPane.showMessageDialog(null,"No se puede eliminar");
            }
        }else{
            JOptionPane.showMessageDialog(null,"Fila no seleccionada");
        }
    }
    
    public void limpiarT(){
        frm.txtNumero_tarea.setText("");
        frm.txtfechaentrega.setText("");
        frm.txtcreado.setText("");
        frm.txtpersonaresponsable.setText("");
        frm.txtnombretarea.setText("");
        frm.txtasunto.setText("");
        frm.txtBuscarT.setText("");
        
    }
   
   /* public void SalirT(){
        System.exit(0);
    }*/
 
    
    public void Reporte(){
        try {
            JasperReport reporte = null;
            String path = "src//Reporte/Reporte_Tarea.jasper";
            reporte= (JasperReport)JRLoader.loadObjectFromFile(path);
            JasperPrint jprint = JasperFillManager.fillReport(path, null,cn);
            JasperViewer view = new JasperViewer(jprint, false);
            view.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            view.setVisible(true);
            
        } catch (JRException ex) {
            Logger.getLogger(conPrincipalModel.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        }
    
    
   
}