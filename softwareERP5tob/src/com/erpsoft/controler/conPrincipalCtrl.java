
package com.erpsoft.controler;

import com.erp.soft.view.conPrincipalVw;
import com.erpsoft.Model.conPrincipalModel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JOptionPane;



public class conPrincipalCtrl implements ActionListener {
  
    private conPrincipalModel modC;
    private conPrincipalVw frm;
    
    public conPrincipalCtrl(conPrincipalModel modC, conPrincipalVw frm){
       
        this.modC= modC;
        this.frm= frm;
        
        this.frm.btnIngresar.addActionListener(this);
        this.frm.btnActualizar.addActionListener(this);
        this.frm.btnCancelar.addActionListener(this);
        this.frm.btnBuscar.addActionListener(this);
        this.frm.btnRefrescartabla.addActionListener(this);
        this.frm.btnEliminar.addActionListener(this);
        this.frm.btnModificar.addActionListener(this);
        /*this.frm.btnSalir.addActionListener(this);*/
        
        
       
        this.frm.btnIngresarT.addActionListener(this);
        this.frm.btnActualizarT.addActionListener(this);
        this.frm.btnCancelarT.addActionListener(this);
        this.frm.btnBuscarT.addActionListener(this);
        this.frm.btnRefrescartablaT.addActionListener(this);
        this.frm.btnEliminarT.addActionListener(this);
        this.frm.btnModificarT.addActionListener(this);
        this.frm.reportebtn.addActionListener(this);
        /*this.frm.btnSalirT.addActionListener(this);*/
    }
    
    public void iniciar(){
            frm.setTitle("Sistema MVC");
            frm.setVisible(true);
            frm.setLocationRelativeTo(null);
            modC.IniciarModelo();
       
    }
    
    @Override
    public void actionPerformed(ActionEvent e){
        if(frm.btnIngresar == e.getSource()){
        try{
            modC.InsertarUsuario();
            
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede guardar");
            }           
        }
        else if(frm.btnRefrescartabla == e.getSource()){
            try{
            modC.mostrarusuario("");
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede mostrar tabla");
            }           
        }
        else if(frm.btnBuscar == e.getSource()){
            try{
            modC.mostrarusuario(frm.txtBuscar.getText());
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede buscar en la tabla");
            }           
        }
        else if(frm.btnModificar == e.getSource()){
            try{
            modC.consultarusuario();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede modificar");
            }           
        }
        else if(frm.btnCancelar == e.getSource()){
            try{
            modC.limpiar();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede limpiar los datos");
            }           
        }
        else if(frm.btnActualizar == e.getSource()){
            try{
            modC.actualizarusuarios();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede actualizar los datos");
            }           
        }
        else if(frm.btnEliminar== e.getSource()){
            try{
            modC.eliminarusuario();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede eliminar los datos");
            }           
        }
        
        
        
            
                                  
        if(frm.btnIngresarT == e.getSource()){
        try{
            modC.InsertarTarea();
           
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede guardar");
            }           
        }
        else if(frm.btnRefrescartablaT == e.getSource()){
            try{
            modC.mostrartarea("");
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede mostrar tabla");
            }           
        }
        else if(frm.btnBuscarT == e.getSource()){
            try{
            modC.mostrartarea(frm.txtBuscarT.getText());
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede buscar en la tabla");
            }           
        }
        else if(frm.btnModificarT == e.getSource()){
            try{
            modC.consultartarea();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede modificar");
            }           
        }
        else if(frm.btnCancelarT == e.getSource()){
            try{
            modC.limpiarT();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede limpiar los datos");
            }           
        }
        
        /*else if(frm.btnSalirT == e.getSource()){
            try{
            modC.SalirT();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "");
            }           
        }*/
        
        else if(frm.btnActualizarT == e.getSource()){
            try{
            modC.actualizartarea();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede actualizar los datos");
            }           
        }
        else if(frm.btnEliminarT== e.getSource()){
            try{
            modC.eliminartarea();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede eliminar los datos");
            }           
        }
        
        else if(frm.reportebtn == e.getSource()){
            try{
            modC.Reporte();
        }catch(Exception ex){
            JOptionPane.showMessageDialog(null, "No se puede mostrar reporte");
            }           
        }
        
        
       }   
        
    }
    
    

