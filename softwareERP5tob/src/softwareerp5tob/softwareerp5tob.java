/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package softwareerp5tob;

import com.erp.soft.view.conPrincipalVw;
import com.erpsoft.Model.conPrincipalModel;
import com.erpsoft.controler.conPrincipalCtrl;
import javax.swing.JOptionPane;
import javax.swing.UIManager;

/**
 *
 * @author Viktor
 */
public class softwareerp5tob {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, e);
        }
       
       conPrincipalModel modC= new conPrincipalModel();
       conPrincipalVw frm= new conPrincipalVw();      
       conPrincipalCtrl ctrl = new conPrincipalCtrl(modC, frm);
       ctrl.iniciar();
       

       
    }
    
}
