
package Softwareerp5tob;
import com.erpsoft.model.conPrincipalMdl;
import com.erpsoft.view.conPrincipalVw;
import com.erpsoft.controller.conPrincipalCtrl;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
public class Softwareerp5tob {

   
    public static void main(String[] args) {
      
        try{
           UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch(Exception e){
        JOptionPane.showMessageDialog(null, e);
    }
        conPrincipalMdl m= new conPrincipalMdl();
        conPrincipalVw v= new conPrincipalVw();
        conPrincipalCtrl c= new conPrincipalCtrl(m,v);
        c.iniciar();
    }
    
}
