
package com.erpsoft.controller;
import com.erpsoft.model.conPrincipalMdl;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import com.erpsoft.view.conPrincipalVw;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
public class conPrincipalCtrl implements ActionListener{
    private final conPrincipalMdl m;    
    private final conPrincipalVw v;

    public conPrincipalCtrl(conPrincipalMdl m, conPrincipalVw v) {
        this.m = m;
        this.v = v;
        conPrincipalVw.btnNuevo.addActionListener(this);
        conPrincipalVw.btnCancelar.addActionListener(this);
        conPrincipalVw.btnAgregar.addActionListener(this);
        conPrincipalVw.btnRefrescar.addActionListener(this);
        conPrincipalVw.btnBuscar.addActionListener(this);
        conPrincipalVw.btnModificar.addActionListener(this);
        conPrincipalVw.btnActualizar.addActionListener(this);
        conPrincipalVw.btnEliminar.addActionListener(this);
        conPrincipalVw.btnNuevo2.addActionListener(this);
        conPrincipalVw.btnCancelar2.addActionListener(this);
        conPrincipalVw.btnAgregar2.addActionListener(this);
        conPrincipalVw.btnRefrescar2.addActionListener(this);
        conPrincipalVw.btnBuscar2.addActionListener(this);
        conPrincipalVw.btnModificar2.addActionListener(this);
        conPrincipalVw.btnActualizar2.addActionListener(this);
        conPrincipalVw.btnEliminar2.addActionListener(this);
        conPrincipalVw.Reportebtn.addActionListener(this);
        conPrincipalVw.CReportebtn.addActionListener(this);
        
    }
    public void iniciar(){
        v.setTitle("Proveedores");
        v.pack();
        v.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        v.setLocationRelativeTo(null);
        v.setVisible(true);
        m.iniciarModelo();
       
        
    }
    @Override
    public void actionPerformed(ActionEvent e){
        if (conPrincipalVw.btnNuevo== e.getSource()) {
            try{
              m.NuevoProveedor();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo agregar");
            }
        }else if (conPrincipalVw.btnCancelar== e.getSource()) {
            try{
              m.Bloqueo();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo Cancelar");  
            }
        }else if (conPrincipalVw.btnAgregar== e.getSource()) {
            try{
              m.AgregarProveedor();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo AGREGAR PEDAZO DE KK");  
            }
        }else if (conPrincipalVw.btnRefrescar== e.getSource()) {
            try{
              m.MostrarProveedores("");
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo refrescar la tabla ");  
            }
        }else if (conPrincipalVw.btnBuscar== e.getSource()) {
            try{
              m.MostrarProveedores(conPrincipalVw.buscartxt1.getText());
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se encontro ese proveedor ");  
            }
        }else if (conPrincipalVw.btnModificar== e.getSource()) {
            try{
              m.Consultarproveedores();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No pudimos consultar  ");  
            }
        }else if (conPrincipalVw.btnActualizar== e.getSource()) {
            try{
              m.ActualizarProveedor();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo actualizar el porveedor  ");  
            }
        }else if (conPrincipalVw.btnEliminar== e.getSource()) {
            try{
              m.EliminarProveeedor();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo eliminar ");  
            }
        }
        //codigo orden de compra
 
        if (conPrincipalVw.btnNuevo2== e.getSource()) {
            try{
              m.Nuevaordendecompra();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo agregar");
            }
        }else if (conPrincipalVw.btnCancelar2== e.getSource()) {
            try{
              m.Bloqueo();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo Cancelar");  
            }
        }else if (conPrincipalVw.btnAgregar2== e.getSource()) {
            try{
              m.Agregarordendecompra();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo AGREGAR ");  
            }
        }else if (conPrincipalVw.btnRefrescar2== e.getSource()) {
            try{
              m.Mostrarordendecompra("");
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se pudo refrescar la tabla ");  
            }
        }else if (conPrincipalVw.btnBuscar2== e.getSource()) {
            try{
              m.Mostrarordendecompra(conPrincipalVw.buscartxt15.getText());
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No se encontro orden de compra ");  
            }
        }else if (conPrincipalVw.btnModificar2== e.getSource()) {
            try{
              m.Consultarordendecompra();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No pudimos consultar  ");  
            }
        }else if (conPrincipalVw.btnActualizar2== e.getSource()) {
            try{
              m.Actualizarordendecompra();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo actualizar orden de compra  ");  
            }
        }else if (conPrincipalVw.btnEliminar2== e.getSource()) {
            try{
              m.Eliminarordendecompra();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo eliminar ");  
            }
        }else if (conPrincipalVw.Reportebtn== e.getSource()) {
            try{
              m.Reporte();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo mostrar el reporte ");  
            }
        }else if (conPrincipalVw.CReportebtn== e.getSource()) {
            try{
              m.ReporteC();
            }catch(Exception ex){
              JOptionPane.showMessageDialog(null,"No Se pudo mostrar el reporte de compras ");  
            }
        }
    } 
}
